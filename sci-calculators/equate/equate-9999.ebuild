# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sci-calculators/equate/equate-9999.ebuild,v 1.4 2005/11/03 01:31:04 vapier Exp $

ESVN_SUB_PROJECT="OLD/MISC"
inherit enlightenment

DESCRIPTION="simple themeable calculator built off of ewl"
HOMEPAGE="http://andy.elcock.org/Software/Equate/"

DEPEND=">=x11-libs/ewl-0.0.4
	>=dev-libs/ecore-0.9.9"
