# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

ESVN_SUB_PROJECT="E-MODULES-EXTRA"
ESVN_URI_APPEND="${PN#e_modules-}"
inherit enlightenment

DESCRIPTION="Taskbar Module. It gives you a taskbar!"

DEPEND=">=x11-wm/enlightenment-0.17.0_alpha:0.17=
	dev-libs/efl"
RDEPEND=${DEPEND}
