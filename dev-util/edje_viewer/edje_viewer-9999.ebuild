# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit enlightenment

DESCRIPTION="edje viewer based on elementary"

DEPEND="dev-libs/ecore
	media-libs/evas
	media-libs/edje
	media-libs/elementary
	dev-libs/eina
	x11-wm/enlightenment"
